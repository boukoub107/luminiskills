package com.luminiskills.web.rest;

import java.util.*;

import com.luminiskills.domain.Message;
import com.luminiskills.service.MessageService;
import com.luminiskills.web.rest.errors.BadRequestAlertException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class MessageResource {

    private final Logger log = LoggerFactory.getLogger(MessageResource.class);

    private static final String ENTITY_NAME = "message";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Autowired
    MessageService messageService;

    public MessageResource() {
    }

    @PostMapping("/messages")
    public ResponseEntity<Message> createExperience(@RequestBody Message message) {
        log.debug("REST request to save Experience : {}", message);
        if (message.getId() != null) {
            throw new BadRequestAlertException("A new experience cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return messageService.save(message);
    }

    @PutMapping("/messages")
    public ResponseEntity<Message> updateExperience(@RequestBody Message message) {
        log.debug("REST request to update Experience : {}", message);
        if (message.getId() == null) {
            throw new BadRequestAlertException("The request cannotInvalid id", ENTITY_NAME, "idnull");
        }
        return messageService.save(message);
    }

    /**
     * {@code GET  /experiences} : get all the experiences.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of experiences in body.
     */
    @GetMapping("/messages")
    public List<Message> getAllExperiences() {
        log.debug("REST request to get all Experiences");
        return messageService.findAll();
    }

    /**
     * {@code GET  /experiences/:id} : get the "id" experience.
     *
     * @param id the id of the experience to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the experience, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/messages/{id}")
    public ResponseEntity<Message> getExperience(@PathVariable Long id) {
        log.debug("REST request to get Experience : {}", id);
        Optional<Message> experience = messageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(experience);
    }

    /**
     * {@code DELETE  /experiences/:id} : delete the "id" experience.
     *
     * @param id the id of the experience to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/messages/{id}")
    public ResponseEntity<Void> deleteExperience(@PathVariable Long id) {
        log.debug("REST request to delete Experience : {}", id);
        messageService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}
