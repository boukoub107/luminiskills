package com.luminiskills.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Offer.
 */
@Entity
@Table(name = "offer")
public class Offer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "last_update")
    private LocalDate lastUpdate;

    @Column(name = "contract_term")
    private String contractTerm;

    @Column(name = "category_id")
    private Integer categoryId;

    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    @Column(name = "activated")
    private Boolean activated;

    @Column(name = "description")
    private String description;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JsonIgnore
    private Recruiter recruiter;

    // @ManyToOne
    // @JsonIgnoreProperties(value = "offers", allowSetters = true)
    // private Application applicationId;

    public Offer() {
    }

    /**
     * @param id
     * @param creationDate
     * @param lastUpdate
     * @param contractTerm
     * @param categoryId
     * @param expirationDate
     * @param activated
     * @param description
     * @param title
     * @param recruiter
     */
    public Offer(Long id, LocalDate creationDate, LocalDate lastUpdate, String contractTerm, Integer categoryId,
            LocalDate expirationDate, Boolean activated, String description, String title, Recruiter recruiter) {
        this.id = id;
        this.creationDate = creationDate;
        this.lastUpdate = lastUpdate;
        this.contractTerm = contractTerm;
        this.categoryId = categoryId;
        this.expirationDate = expirationDate;
        this.activated = activated;
        this.description = description;
        this.title = title;
        this.recruiter = recruiter;
    }

    public Offer(LocalDate creationDate, LocalDate lastUpdate, String contractTerm, Integer categoryId,
            LocalDate expirationDate, Boolean activated, String description, String title, Recruiter recruiter) {
        this.creationDate = creationDate;
        this.lastUpdate = lastUpdate;
        this.contractTerm = contractTerm;
        this.categoryId = categoryId;
        this.expirationDate = expirationDate;
        this.activated = activated;
        this.description = description;
        this.title = title;
        this.recruiter = recruiter;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the activated
     */
    public Boolean getActivated() {
        return activated;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Offer creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public Offer lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getContractTerm() {
        return contractTerm;
    }

    public Offer contractTerm(String contractTerm) {
        this.contractTerm = contractTerm;
        return this;
    }

    public void setContractTerm(String contractTerm) {
        this.contractTerm = contractTerm;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Offer categoryId(Integer categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public Offer expirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Boolean isActivated() {
        return activated;
    }

    public Offer activated(Boolean activated) {
        this.activated = activated;
        return this;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public String getDescription() {
        return description;
    }

    public Offer description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Recruiter getRecruiter() {
        return recruiter;
    }

    public void setRecruiter(Recruiter recruiter) {
        this.recruiter = recruiter;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((activated == null) ? 0 : activated.hashCode());
        result = prime * result + ((categoryId == null) ? 0 : categoryId.hashCode());
        result = prime * result + ((contractTerm == null) ? 0 : contractTerm.hashCode());
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((expirationDate == null) ? 0 : expirationDate.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((lastUpdate == null) ? 0 : lastUpdate.hashCode());
        result = prime * result + ((recruiter == null) ? 0 : recruiter.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Offer other = (Offer) obj;
        if (activated == null) {
            if (other.activated != null)
                return false;
        } else if (!activated.equals(other.activated))
            return false;
        if (categoryId == null) {
            if (other.categoryId != null)
                return false;
        } else if (!categoryId.equals(other.categoryId))
            return false;
        if (contractTerm == null) {
            if (other.contractTerm != null)
                return false;
        } else if (!contractTerm.equals(other.contractTerm))
            return false;
        if (creationDate == null) {
            if (other.creationDate != null)
                return false;
        } else if (!creationDate.equals(other.creationDate))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (expirationDate == null) {
            if (other.expirationDate != null)
                return false;
        } else if (!expirationDate.equals(other.expirationDate))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (lastUpdate == null) {
            if (other.lastUpdate != null)
                return false;
        } else if (!lastUpdate.equals(other.lastUpdate))
            return false;
        if (recruiter == null) {
            if (other.recruiter != null)
                return false;
        } else if (!recruiter.equals(other.recruiter))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        return "Offer [activated=" + activated + ", categoryId=" + categoryId + ", contractTerm=" + contractTerm
                + ", creationDate=" + creationDate + ", description=" + description + ", expirationDate="
                + expirationDate + ", id=" + id + ", lastUpdate=" + lastUpdate + ", recruiter=" + recruiter + ", title="
                + title + "]";
    }

    // public Application getApplicationId() {
    // return applicationId;
    // }

    // public Offer applicationId(Application application) {
    // this.applicationId = application;
    // return this;
    // }

    // public void setApplicationId(Application application) {
    // this.applicationId = application;
    // }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

}
