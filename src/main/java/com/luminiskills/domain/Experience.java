package com.luminiskills.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Experience.
 */
@Entity
@Table(name = "experience")
public class Experience implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "institution")
    private String institution;

    @Column(name = "contract")
    private String contract;

    @Column(name = "from_date")
    private LocalDate fromDate;

    @Column(name = "to_date")
    private LocalDate toDate;

    @Column(name = "location")
    private String location;

    @Column(name = "skills")
    private String skills;

    @Column(name = "level")
    private String level;

    @ManyToOne
    @JsonIgnore
    private Candidate candidate;

    public Experience() {
    }

    public Experience(Long id, String title, String institution, String contract, LocalDate fromDate, LocalDate toDate,
            String location, String skills, String level, Candidate candidate) {
        this.id = id;
        this.title = title;
        this.institution = institution;
        this.contract = contract;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.location = location;
        this.skills = skills;
        this.level = level;
        this.candidate = candidate;
    }

    public Experience(String title, String institution, String contract, LocalDate fromDate, LocalDate toDate,
            String location, String skills, String level, Candidate candidate) {
        this.title = title;
        this.institution = institution;
        this.contract = contract;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.location = location;
        this.skills = skills;
        this.level = level;
        this.candidate = candidate;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Experience title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstitution() {
        return institution;
    }

    public Experience institution(String institution) {
        this.institution = institution;
        return this;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getContract() {
        return contract;
    }

    public Experience contract(String contract) {
        this.contract = contract;
        return this;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public Experience fromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public Experience toDate(LocalDate toDate) {
        this.toDate = toDate;
        return this;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public String getLocation() {
        return location;
    }

    public Experience location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSkills() {
        return skills;
    }

    public Experience skills(String skills) {
        this.skills = skills;
        return this;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getLevel() {
        return level;
    }

    public Experience level(String level) {
        this.level = level;
        return this;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Experience)) {
            return false;
        }
        return id != null && id.equals(((Experience) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Experience{" + "id=" + getId() + ", title='" + getTitle() + "'" + ", institution='" + getInstitution()
                + "'" + ", contract='" + getContract() + "'" + ", fromDate='" + getFromDate() + "'" + ", toDate='"
                + getToDate() + "'" + ", location='" + getLocation() + "'" + ", skills='" + getSkills() + "'"
                + ", level='" + getLevel() + "'" + "}";
    }
}
