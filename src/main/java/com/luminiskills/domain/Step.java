package com.luminiskills.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Step.
 */
@Entity
@Table(name = "step")
public class Step implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "close_date")
    private LocalDate closeDate;

    @OneToOne
    @JoinColumn(unique = true)
    private Application application;

    public Step() {
    }

    public Step(String name, LocalDate creationDate, LocalDate closeDate, Application application) {
        this.name = name;
        this.creationDate = creationDate;
        this.closeDate = closeDate;
        this.application = application;
    }

    public Step(Long id, String name, LocalDate creationDate, LocalDate closeDate, Application application) {
        this.id = id;
        this.name = name;
        this.creationDate = creationDate;
        this.closeDate = closeDate;
        this.application = application;
    }

    public Step(String name, LocalDate creationDate, LocalDate closeDate) {
        this.name = name;
        this.creationDate = creationDate;
        this.closeDate = closeDate;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Step name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Step creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getCloseDate() {
        return closeDate;
    }

    public Step closeDate(LocalDate closeDate) {
        this.closeDate = closeDate;
        return this;
    }

    public void setCloseDate(LocalDate closeDate) {
        this.closeDate = closeDate;
    }

    public Application getApplication() {
        return application;
    }

    public Step application(Application application) {
        this.application = application;
        return this;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Step)) {
            return false;
        }
        return id != null && id.equals(((Step) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Step{" + "id=" + getId() + ", name='" + getName() + "'" + ", creationDate='" + getCreationDate() + "'"
                + ", closeDate='" + getCloseDate() + "'" + "}";
    }
}
