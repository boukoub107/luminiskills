package com.luminiskills.service;

import com.luminiskills.domain.Recruiter;
import com.luminiskills.service.dto.RecruiterDTO;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Recruiter}.
 */
public interface RecruiterService {

    /**
     * Save a recruiter.
     *
     * @param recruiter the entity to save.
     * @return the persisted entity.
     */
    ResponseEntity<Recruiter> save(RecruiterDTO recruiter);
    ResponseEntity<Recruiter> update(RecruiterDTO recruiter);

    /**
     * Get all the recruiters.
     *
     * @return the list of entities.
     */
    List<Recruiter> findAll();


    /**
     * Get the "id" recruiter.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Recruiter> findOne(Long id);

    /**
     * Delete the "id" recruiter.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
