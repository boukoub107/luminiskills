package com.luminiskills.service.dto;

import java.time.LocalDate;
import java.util.*;

public class CandidateDTO {

    private Long id;
    private String driverslicense;
    private String mobility;
    private String city;
    private String address;
    private String cv;
    private LocalDate bithDate;
    private Boolean handicapSituation;
    private Boolean fullProfil;
    private Boolean withoutExperience;
    private Long user;
    private Set<Long> applications = new HashSet<>();
    private Set<Long> experiences = new HashSet<>();
    private Set<Long> trainings = new HashSet<>();

    public CandidateDTO() {
    }

    /**
     * @param id
     * @param driverslicense
     * @param mobility
     * @param city
     * @param address
     * @param cv
     * @param bithDate
     * @param handicapSituation
     * @param fullProfil
     * @param withoutExperience
     * @param user
     */
    public CandidateDTO(String driverslicense, String mobility, String city, String address, String cv,
            LocalDate bithDate, Boolean handicapSituation, Boolean fullProfil, Boolean withoutExperience, Long user) {
        this.driverslicense = driverslicense;
        this.mobility = mobility;
        this.city = city;
        this.address = address;
        this.cv = cv;
        this.bithDate = bithDate;
        this.handicapSituation = handicapSituation;
        this.fullProfil = fullProfil;
        this.withoutExperience = withoutExperience;
        this.user = user;
    }

    public CandidateDTO(String driverslicense, String mobility, String city, String address, String cv,
            LocalDate bithDate, Boolean handicapSituation, Boolean fullProfil, Boolean withoutExperience,
            Set<Long> applications, Set<Long> experiences, Set<Long> trainings) {
        this.driverslicense = driverslicense;
        this.mobility = mobility;
        this.city = city;
        this.address = address;
        this.cv = cv;
        this.bithDate = bithDate;
        this.handicapSituation = handicapSituation;
        this.fullProfil = fullProfil;
        this.withoutExperience = withoutExperience;
        this.applications = applications;
        this.experiences = experiences;
        this.trainings = trainings;
    }

    public CandidateDTO(String driverslicense, String mobility, String city, String address, String cv,
            LocalDate bithDate, Boolean handicapSituation, Boolean fullProfil, Boolean withoutExperience) {
        this.driverslicense = driverslicense;
        this.mobility = mobility;
        this.city = city;
        this.address = address;
        this.cv = cv;
        this.bithDate = bithDate;
        this.handicapSituation = handicapSituation;
        this.fullProfil = fullProfil;
        this.withoutExperience = withoutExperience;
    }

    /**
     * @param id
     * @param driverslicense
     * @param mobility
     * @param city
     * @param address
     * @param cv
     * @param bithDate
     * @param handicapSituation
     * @param fullProfil
     * @param withoutExperience
     * @param user
     * @param applications
     * @param experiences
     * @param trainings
     */
    public CandidateDTO(Long id, String driverslicense, String mobility, String city, String address, String cv,
            LocalDate bithDate, Boolean handicapSituation, Boolean fullProfil, Boolean withoutExperience, Long user,
            Set<Long> applications, Set<Long> experiences, Set<Long> trainings) {
        this.id = id;
        this.driverslicense = driverslicense;
        this.mobility = mobility;
        this.city = city;
        this.address = address;
        this.cv = cv;
        this.bithDate = bithDate;
        this.handicapSituation = handicapSituation;
        this.fullProfil = fullProfil;
        this.withoutExperience = withoutExperience;
        this.user = user;
        this.applications = applications;
        this.experiences = experiences;
        this.trainings = trainings;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDriverslicense() {
        return driverslicense;
    }

    public void setDriverslicense(String driverslicense) {
        this.driverslicense = driverslicense;
    }

    /**
     * @return the user
     */
    public Long getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(Long user) {
        this.user = user;
    }

    public String getMobility() {
        return mobility;
    }

    public void setMobility(String mobility) {
        this.mobility = mobility;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public LocalDate getBithDate() {
        return bithDate;
    }

    public void setBithDate(LocalDate bithDate) {
        this.bithDate = bithDate;
    }

    public Boolean getHandicapSituation() {
        return handicapSituation;
    }

    public void setHandicapSituation(Boolean handicapSituation) {
        this.handicapSituation = handicapSituation;
    }

    public Boolean getFullProfil() {
        return fullProfil;
    }

    public void setFullProfil(Boolean fullProfil) {
        this.fullProfil = fullProfil;
    }

    public Boolean getWithoutExperience() {
        return withoutExperience;
    }

    public void setWithoutExperience(Boolean withoutExperience) {
        this.withoutExperience = withoutExperience;
    }

    public Set<Long> getApplications() {
        return applications;
    }

    public void setApplications(Set<Long> applications) {
        this.applications = applications;
    }

    public Set<Long> getExperiences() {
        return experiences;
    }

    public void setExperiences(Set<Long> experiences) {
        this.experiences = experiences;
    }

    public Set<Long> getTrainings() {
        return trainings;
    }

    public void setTrainings(Set<Long> trainings) {
        this.trainings = trainings;
    }

}
