package com.luminiskills.service.dto;

import java.time.LocalDate;

public class ApplicationDTO {

    private Long id;

    private LocalDate creationDate;

    private Boolean newApplication;

    private String state;

    private String comment;

    private Long step;

    private Long offer;

    private Long candidate;

    public ApplicationDTO() {
    }

    /**
     * @param id
     * @param creationDate
     * @param newApplication
     * @param state
     * @param comment
     * @param step
     * @param offer
     * @param candidate
     */
    public ApplicationDTO(Long id, LocalDate creationDate, Boolean newApplication, String state, String comment,
            Long step, Long offer, Long candidate) {
        this.id = id;
        this.creationDate = creationDate;
        this.newApplication = newApplication;
        this.state = state;
        this.comment = comment;
        this.step = step;
        this.offer = offer;
        this.candidate = candidate;
    }

    public ApplicationDTO(LocalDate creationDate, Boolean newApplication, String state, String comment, Long step,
            Long offer, Long candidate) {
        this.creationDate = creationDate;
        this.newApplication = newApplication;
        this.state = state;
        this.comment = comment;
        this.step = step;
        this.offer = offer;
        this.candidate = candidate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getNewApplication() {
        return newApplication;
    }

    public void setNewApplication(Boolean newApplication) {
        this.newApplication = newApplication;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getStep() {
        return step;
    }

    public void setStep(Long step) {
        this.step = step;
    }

    public Long getOffer() {
        return offer;
    }

    public void setOffer(Long offer) {
        this.offer = offer;
    }

    public Long getCandidate() {
        return candidate;
    }

    public void setCandidate(Long candidate) {
        this.candidate = candidate;
    }

    @Override
    public String toString() {
        return "ApplicationDTO [candidate=" + candidate + ", comment=" + comment + ", creationDate=" + creationDate
                + ", id=" + id + ", newApplication=" + newApplication + ", offer=" + offer + ", state=" + state
                + ", stepId=" + step + "]";
    }
}
