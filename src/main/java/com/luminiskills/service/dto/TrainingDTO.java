package com.luminiskills.service.dto;

import java.time.LocalDate;

public class TrainingDTO {

    private Long id;
    private String diploma;
    private String university;
    private LocalDate fromDate;
    private LocalDate toDate;
    private String city;
    private Long candidate;

    public TrainingDTO() {
    }

    /**
     * @param id
     * @param diploma
     * @param university
     * @param fromDate
     * @param toDate
     * @param city
     * @param candidate
     */
    public TrainingDTO(Long id, String diploma, String university, LocalDate fromDate, LocalDate toDate, String city,
            Long candidate) {
        this.id = id;
        this.diploma = diploma;
        this.university = university;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.city = city;
        this.candidate = candidate;
    }

    public TrainingDTO(String diploma, String university, LocalDate fromDate, LocalDate toDate, String city,
            Long candidate) {
        this.diploma = diploma;
        this.university = university;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.city = city;
        this.candidate = candidate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiploma() {
        return diploma;
    }

    public void setDiploma(String diploma) {
        this.diploma = diploma;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getCandidate() {
        return candidate;
    }

    public void setCandidate(Long candidate) {
        this.candidate = candidate;
    }

}
