package com.luminiskills.service.dto;

import java.time.LocalDate;

public class ExperienceDTO {

    private Long id;

    private String title;

    private String institution;

    private String contract;

    private LocalDate fromDate;

    private LocalDate toDate;

    private String location;

    private String skills;

    private String level;

    private Long candidate;

    public ExperienceDTO() {
    }

    /**
     * @param id
     * @param title
     * @param institution
     * @param contract
     * @param fromDate
     * @param toDate
     * @param location
     * @param skills
     * @param level
     * @param candidate
     */
    public ExperienceDTO(Long id, String title, String institution, String contract, LocalDate fromDate,
            LocalDate toDate, String location, String skills, String level, Long candidate) {
        this.id = id;
        this.title = title;
        this.institution = institution;
        this.contract = contract;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.location = location;
        this.skills = skills;
        this.level = level;
        this.candidate = candidate;
    }

    public ExperienceDTO(String title, String institution, String contract, LocalDate fromDate, LocalDate toDate,
            String location, String skills, String level, Long candidate) {
        this.title = title;
        this.institution = institution;
        this.contract = contract;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.location = location;
        this.skills = skills;
        this.level = level;
        this.candidate = candidate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Long getCandidate() {
        return candidate;
    }

    public void setCandidate(Long candidate) {
        this.candidate = candidate;
    }

}
