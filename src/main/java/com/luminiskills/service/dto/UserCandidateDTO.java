package com.luminiskills.service.dto;

import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

public class UserCandidateDTO {
    
    private Long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String imageUrl;
    private boolean activated = false;
    private String langKey;
    private String createdBy;
    private Instant createdDate;
    private String lastModifiedBy;
    private Instant lastModifiedDate;
    private Set<String> authorities;

    private String driverslicense;
    private String mobility;
    private String city;
    private String address;
    private String cv;
    private LocalDate bithDate;
    private Boolean handicapSituation;
    private Boolean fullProfil;
    private Boolean withoutExperience;
    
    public UserCandidateDTO() {
    }

    public UserCandidateDTO(String login, String password, String firstName, String lastName, String email,
            String imageUrl, boolean activated, String langKey, String createdBy, Instant createdDate,
            String lastModifiedBy, Instant lastModifiedDate, Set<String> authorities, String driverslicense,
            String mobility, String city, String address, String cv, LocalDate bithDate, Boolean handicapSituation,
            Boolean fullProfil, Boolean withoutExperience) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imageUrl = imageUrl;
        this.activated = activated;
        this.langKey = langKey;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.authorities = authorities;
        this.driverslicense = driverslicense;
        this.mobility = mobility;
        this.city = city;
        this.address = address;
        this.cv = cv;
        this.bithDate = bithDate;
        this.handicapSituation = handicapSituation;
        this.fullProfil = fullProfil;
        this.withoutExperience = withoutExperience;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

    public String getDriverslicense() {
        return driverslicense;
    }

    public void setDriverslicense(String driverslicense) {
        this.driverslicense = driverslicense;
    }

    public String getMobility() {
        return mobility;
    }

    public void setMobility(String mobility) {
        this.mobility = mobility;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public LocalDate getBithDate() {
        return bithDate;
    }

    public void setBithDate(LocalDate bithDate) {
        this.bithDate = bithDate;
    }

    public Boolean getHandicapSituation() {
        return handicapSituation;
    }

    public void setHandicapSituation(Boolean handicapSituation) {
        this.handicapSituation = handicapSituation;
    }

    public Boolean getFullProfil() {
        return fullProfil;
    }

    public void setFullProfil(Boolean fullProfil) {
        this.fullProfil = fullProfil;
    }

    public Boolean getWithoutExperience() {
        return withoutExperience;
    }

    public void setWithoutExperience(Boolean withoutExperience) {
        this.withoutExperience = withoutExperience;
    }

}
