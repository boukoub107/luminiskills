package com.luminiskills.service.dto;

import java.time.LocalDate;

public class OfferDTO {

    private Long id;

    private LocalDate creationDate;

    private LocalDate lastUpdate;

    private String contractTerm;

    private String title;

    private Integer categoryId;

    private LocalDate expirationDate;

    private Boolean activated;

    private String description;

    private Long recruiter;

    public OfferDTO() {
    }

    /**
     * @param id
     * @param creationDate
     * @param lastUpdate
     * @param contractTerm
     * @param title
     * @param categoryId
     * @param expirationDate
     * @param activated
     * @param description
     * @param recruiter
     */
    public OfferDTO(Long id, LocalDate creationDate, LocalDate lastUpdate, String contractTerm, String title,
            Integer categoryId, LocalDate expirationDate, Boolean activated, String description, Long recruiter) {
        this.id = id;
        this.creationDate = creationDate;
        this.lastUpdate = lastUpdate;
        this.contractTerm = contractTerm;
        this.title = title;
        this.categoryId = categoryId;
        this.expirationDate = expirationDate;
        this.activated = activated;
        this.description = description;
        this.recruiter = recruiter;
    }

    /**
     * @param creationDate
     * @param lastUpdate
     * @param contractTerm
     * @param title
     * @param categoryId
     * @param expirationDate
     * @param activated
     * @param description
     * @param recruiter
     */
    public OfferDTO(LocalDate creationDate, LocalDate lastUpdate, String contractTerm, String title, Integer categoryId,
            LocalDate expirationDate, Boolean activated, String description, Long recruiter) {
        this.creationDate = creationDate;
        this.lastUpdate = lastUpdate;
        this.contractTerm = contractTerm;
        this.title = title;
        this.categoryId = categoryId;
        this.expirationDate = expirationDate;
        this.activated = activated;
        this.description = description;
        this.recruiter = recruiter;
    }

    @Override
    public String toString() {
        return "OfferDTO [activated=" + activated + ", categoryId=" + categoryId + ", contractTerm=" + contractTerm
                + ", creationDate=" + creationDate + ", description=" + description + ", expirationDate="
                + expirationDate + ", id=" + id + ", lastUpdate=" + lastUpdate + ", recruiter=" + recruiter + "]";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getContractTerm() {
        return contractTerm;
    }

    public void setContractTerm(String contractTerm) {
        this.contractTerm = contractTerm;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getRecruiter() {
        return recruiter;
    }

    public void setRecruiter(Long recruiter) {
        this.recruiter = recruiter;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

}
