package com.luminiskills.service.dto;

import java.util.*;

public class RecruiterDTO {

    private Long id;
    private String post;
    private Set<Long> offers = new HashSet<>();
    private Long company;

    public RecruiterDTO() {
    }

    /**
     * @param id
     * @param post
     * @param offers
     * @param company
     */
    public RecruiterDTO(Long id, String post, Set<Long> offers, Long company) {
        this.id = id;
        this.post = post;
        this.offers = offers;
        this.company = company;
    }

    public RecruiterDTO(String post, Set<Long> offers, Long company) {
        this.post = post;
        this.offers = offers;
        this.company = company;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Set<Long> getOffers() {
        return offers;
    }

    public void setOffers(Set<Long> offers) {
        this.offers = offers;
    }

    public Long getCompany() {
        return company;
    }

    public void setCompany(Long company) {
        this.company = company;
    }

}
