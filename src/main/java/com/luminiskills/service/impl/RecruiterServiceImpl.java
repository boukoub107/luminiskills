package com.luminiskills.service.impl;

import com.luminiskills.service.CompanyService;
import com.luminiskills.service.OfferService;
import com.luminiskills.service.RecruiterService;
import com.luminiskills.service.dto.RecruiterDTO;
import com.luminiskills.domain.Company;
import com.luminiskills.domain.Offer;
import com.luminiskills.domain.Recruiter;
import com.luminiskills.repository.RecruiterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Service Implementation for managing {@link Recruiter}.
 */
@Service
@Transactional
public class RecruiterServiceImpl implements RecruiterService {

    private final Logger log = LoggerFactory.getLogger(RecruiterServiceImpl.class);

    private final RecruiterRepository recruiterRepository;

    @Autowired
    CompanyService companyService;

    @Autowired
    OfferService offerService;

    public RecruiterServiceImpl(RecruiterRepository recruiterRepository) {
        this.recruiterRepository = recruiterRepository;
    }

    @Override
    public ResponseEntity<Recruiter> save(RecruiterDTO recruiter) {
        log.debug("Request to save Recruiter : {}", recruiter);
        if (recruiter.getCompany() == null) {
            return new ResponseEntity<Recruiter>(HttpStatus.BAD_REQUEST);
        }

        Company company = companyService.findOne(recruiter.getCompany()).get();
        Recruiter rc = new Recruiter(recruiter.getPost(), company);
        return new ResponseEntity<Recruiter>(recruiterRepository.save(rc), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Recruiter> update(RecruiterDTO recruiter) {
        log.debug("Request to save Recruiter : {}", recruiter);
        if (recruiter.getCompany() == null) {
            return new ResponseEntity<Recruiter>(HttpStatus.BAD_REQUEST);
        }

        Set<Offer> offers = new HashSet<>();
        for (Long offer : recruiter.getOffers()) {
            offers.add(offerService.findOne(offer).get());
        }

        Company company = companyService.findOne(recruiter.getCompany()).get();
        Recruiter rc = new Recruiter(recruiter.getId(), recruiter.getPost(), offers, company);
        return new ResponseEntity<Recruiter>(recruiterRepository.save(rc), HttpStatus.CREATED);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Recruiter> findAll() {
        log.debug("Request to get all Recruiters");
        return recruiterRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Recruiter> findOne(Long id) {
        log.debug("Request to get Recruiter : {}", id);
        return recruiterRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Recruiter : {}", id);
        recruiterRepository.deleteById(id);
    }
}
