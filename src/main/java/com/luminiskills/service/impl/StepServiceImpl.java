package com.luminiskills.service.impl;

import com.luminiskills.service.ApplicationService;
import com.luminiskills.service.StepService;
import com.luminiskills.service.dto.StepDTO;
import com.luminiskills.domain.Application;
import com.luminiskills.domain.Step;
import com.luminiskills.repository.StepRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Step}.
 */
@Service
@Transactional
public class StepServiceImpl implements StepService {

    private final Logger log = LoggerFactory.getLogger(StepServiceImpl.class);

    private final StepRepository stepRepository;

    @Autowired
    ApplicationService applicationService;

    public StepServiceImpl(StepRepository stepRepository) {
        this.stepRepository = stepRepository;
    }

    @Override
    public ResponseEntity<Step> save(StepDTO step) {
        log.debug("Request to save Step : {}", step);

        Step stp = new Step(step.getName(), step.getCreationDate(), step.getCloseDate());

        return new ResponseEntity<Step>(stepRepository.save(stp), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Step> update(StepDTO step) {
        log.debug("Request to save Step : {}", step);
        Application application = applicationService.findOne(step.getApplication()).get();

        Step stp = new Step(step.getId(), step.getName(), step.getCreationDate(), step.getCloseDate(), application);

        return new ResponseEntity<Step>(stepRepository.save(stp), HttpStatus.CREATED);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Step> findAll() {
        log.debug("Request to get all Steps");
        return stepRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Step> findOne(Long id) {
        log.debug("Request to get Step : {}", id);
        return stepRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Step : {}", id);
        stepRepository.deleteById(id);
    }
}
