package com.luminiskills.service.impl;

import com.luminiskills.service.CompanyService;
import com.luminiskills.service.RecruiterService;
import com.luminiskills.service.dto.CompanyDTO;
import com.luminiskills.domain.Company;
import com.luminiskills.domain.Recruiter;
import com.luminiskills.repository.CompanyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Service Implementation for managing {@link Company}.
 */
@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {

    private final Logger log = LoggerFactory.getLogger(CompanyServiceImpl.class);

    private final CompanyRepository companyRepository;

    @Autowired
    RecruiterService recruiterService;

    public CompanyServiceImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public ResponseEntity<Company> save(CompanyDTO company) {
        log.debug("Request to save Company : {}", company);
        Company cmp = new Company(company.getLogo(), company.getName(), company.getAddress(), company.getPostalCode(),
                company.getCity(), company.getCountry());
        return new ResponseEntity<Company>(companyRepository.save(cmp), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Company> update(CompanyDTO company) {
        log.debug("Request to save Company : {}", company);
        Set<Recruiter> recruiters = new HashSet<>();
        if (company.getRecruiters() != null) {
            for (Long rc : company.getRecruiters()) {
                recruiters.add(recruiterService.findOne(rc).get());
            }
        }
        Company cmp = new Company(company.getId(), company.getLogo(), company.getName(), company.getAddress(),
                company.getPostalCode(), company.getCity(), company.getCountry(), recruiters);
        return new ResponseEntity<Company>(companyRepository.save(cmp), HttpStatus.CREATED);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Company> findAll() {
        log.debug("Request to get all Companies");
        return companyRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Company> findOne(Long id) {
        log.debug("Request to get Company : {}", id);
        return companyRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Company : {}", id);
        companyRepository.deleteById(id);
    }
}
