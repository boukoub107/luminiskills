package com.luminiskills.service;

import java.util.*;

import com.luminiskills.domain.Message;

import org.springframework.http.ResponseEntity;

public interface MessageService {
    /**
     * Save a experience.
     *
     * @param experience the entity to save.
     * @return the persisted entity.
     */
    ResponseEntity<Message> save(Message message);

    /**
     * Get all the experiences.
     *
     * @return the list of entities.
     */
    List<Message> findAll();

    /**
     * Get the "id" experience.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Message> findOne(Long id);

    /**
     * Delete the "id" experience.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
