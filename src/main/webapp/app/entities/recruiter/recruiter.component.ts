import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IRecruiter } from 'app/shared/model/recruiter.model';
import { RecruiterService } from './recruiter.service';
import { RecruiterDeleteDialogComponent } from './recruiter-delete-dialog.component';

@Component({
  selector: 'jhi-recruiter',
  templateUrl: './recruiter.component.html',
})
export class RecruiterComponent implements OnInit, OnDestroy {
  recruiters?: IRecruiter[];
  eventSubscriber?: Subscription;

  constructor(protected recruiterService: RecruiterService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.recruiterService.query().subscribe((res: HttpResponse<IRecruiter[]>) => (this.recruiters = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInRecruiters();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IRecruiter): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInRecruiters(): void {
    this.eventSubscriber = this.eventManager.subscribe('recruiterListModification', () => this.loadAll());
  }

  delete(recruiter: IRecruiter): void {
    const modalRef = this.modalService.open(RecruiterDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.recruiter = recruiter;
  }
}
