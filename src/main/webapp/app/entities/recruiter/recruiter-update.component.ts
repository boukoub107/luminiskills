import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRecruiter, Recruiter } from 'app/shared/model/recruiter.model';
import { RecruiterService } from './recruiter.service';
import { ICompany } from 'app/shared/model/company.model';
import { CompanyService } from 'app/entities/company/company.service';

@Component({
  selector: 'jhi-recruiter-update',
  templateUrl: './recruiter-update.component.html',
})
export class RecruiterUpdateComponent implements OnInit {
  isSaving = false;
  companies: ICompany[] = [];

  editForm = this.fb.group({
    id: [],
    post: [],
    companyId: [],
  });

  constructor(
    protected recruiterService: RecruiterService,
    protected companyService: CompanyService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recruiter }) => {
      this.updateForm(recruiter);

      this.companyService.query().subscribe((res: HttpResponse<ICompany[]>) => (this.companies = res.body || []));
    });
  }

  updateForm(recruiter: IRecruiter): void {
    this.editForm.patchValue({
      id: recruiter.id,
      post: recruiter.post,
      companyId: recruiter.companyId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const recruiter = this.createFromForm();
    if (recruiter.id !== undefined) {
      this.subscribeToSaveResponse(this.recruiterService.update(recruiter));
    } else {
      this.subscribeToSaveResponse(this.recruiterService.create(recruiter));
    }
  }

  private createFromForm(): IRecruiter {
    return {
      ...new Recruiter(),
      id: this.editForm.get(['id'])!.value,
      post: this.editForm.get(['post'])!.value,
      companyId: this.editForm.get(['companyId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRecruiter>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICompany): any {
    return item.id;
  }
}
