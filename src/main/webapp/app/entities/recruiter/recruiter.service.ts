import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRecruiter } from 'app/shared/model/recruiter.model';

type EntityResponseType = HttpResponse<IRecruiter>;
type EntityArrayResponseType = HttpResponse<IRecruiter[]>;

@Injectable({ providedIn: 'root' })
export class RecruiterService {
  public resourceUrl = SERVER_API_URL + 'api/recruiters';

  constructor(protected http: HttpClient) {}

  create(recruiter: IRecruiter): Observable<EntityResponseType> {
    return this.http.post<IRecruiter>(this.resourceUrl, recruiter, { observe: 'response' });
  }

  update(recruiter: IRecruiter): Observable<EntityResponseType> {
    return this.http.put<IRecruiter>(this.resourceUrl, recruiter, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRecruiter>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRecruiter[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
