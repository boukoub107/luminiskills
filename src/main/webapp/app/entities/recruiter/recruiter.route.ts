import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRecruiter, Recruiter } from 'app/shared/model/recruiter.model';
import { RecruiterService } from './recruiter.service';
import { RecruiterComponent } from './recruiter.component';
import { RecruiterDetailComponent } from './recruiter-detail.component';
import { RecruiterUpdateComponent } from './recruiter-update.component';

@Injectable({ providedIn: 'root' })
export class RecruiterResolve implements Resolve<IRecruiter> {
  constructor(private service: RecruiterService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRecruiter> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((recruiter: HttpResponse<Recruiter>) => {
          if (recruiter.body) {
            return of(recruiter.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Recruiter());
  }
}

export const recruiterRoute: Routes = [
  {
    path: '',
    component: RecruiterComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'luminiskillsApp.recruiter.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RecruiterDetailComponent,
    resolve: {
      recruiter: RecruiterResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'luminiskillsApp.recruiter.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RecruiterUpdateComponent,
    resolve: {
      recruiter: RecruiterResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'luminiskillsApp.recruiter.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RecruiterUpdateComponent,
    resolve: {
      recruiter: RecruiterResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'luminiskillsApp.recruiter.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
