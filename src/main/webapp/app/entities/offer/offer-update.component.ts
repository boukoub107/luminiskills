import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOffer, Offer } from 'app/shared/model/offer.model';
import { OfferService } from './offer.service';
import { IRecruiter } from 'app/shared/model/recruiter.model';
import { RecruiterService } from 'app/entities/recruiter/recruiter.service';
import { IApplication } from 'app/shared/model/application.model';
import { ApplicationService } from 'app/entities/application/application.service';

type SelectableEntity = IRecruiter | IApplication;

@Component({
  selector: 'jhi-offer-update',
  templateUrl: './offer-update.component.html',
})
export class OfferUpdateComponent implements OnInit {
  isSaving = false;
  recruiters: IRecruiter[] = [];
  applications: IApplication[] = [];
  creationDateDp: any;
  lastUpdateDp: any;
  expirationDateDp: any;

  editForm = this.fb.group({
    id: [],
    creationDate: [],
    lastUpdate: [],
    contractTerm: [],
    categoryId: [],
    expirationDate: [],
    activated: [],
    description: [],
    recruiterId: [],
    applicationId: [],
  });

  constructor(
    protected offerService: OfferService,
    protected recruiterService: RecruiterService,
    protected applicationService: ApplicationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ offer }) => {
      this.updateForm(offer);

      this.recruiterService.query().subscribe((res: HttpResponse<IRecruiter[]>) => (this.recruiters = res.body || []));

      this.applicationService.query().subscribe((res: HttpResponse<IApplication[]>) => (this.applications = res.body || []));
    });
  }

  updateForm(offer: IOffer): void {
    this.editForm.patchValue({
      id: offer.id,
      creationDate: offer.creationDate,
      lastUpdate: offer.lastUpdate,
      contractTerm: offer.contractTerm,
      categoryId: offer.categoryId,
      expirationDate: offer.expirationDate,
      activated: offer.activated,
      description: offer.description,
      recruiterId: offer.recruiterId,
      applicationId: offer.applicationId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const offer = this.createFromForm();
    if (offer.id !== undefined) {
      this.subscribeToSaveResponse(this.offerService.update(offer));
    } else {
      this.subscribeToSaveResponse(this.offerService.create(offer));
    }
  }

  private createFromForm(): IOffer {
    return {
      ...new Offer(),
      id: this.editForm.get(['id'])!.value,
      creationDate: this.editForm.get(['creationDate'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      contractTerm: this.editForm.get(['contractTerm'])!.value,
      categoryId: this.editForm.get(['categoryId'])!.value,
      expirationDate: this.editForm.get(['expirationDate'])!.value,
      activated: this.editForm.get(['activated'])!.value,
      description: this.editForm.get(['description'])!.value,
      recruiterId: this.editForm.get(['recruiterId'])!.value,
      applicationId: this.editForm.get(['applicationId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOffer>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
