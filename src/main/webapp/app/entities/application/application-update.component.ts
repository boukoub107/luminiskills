import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IApplication, Application } from 'app/shared/model/application.model';
import { ApplicationService } from './application.service';
import { IStep } from 'app/shared/model/step.model';
import { StepService } from 'app/entities/step/step.service';
import { ICandidate } from 'app/shared/model/candidate.model';
import { CandidateService } from 'app/entities/candidate/candidate.service';

type SelectableEntity = IStep | ICandidate;

@Component({
  selector: 'jhi-application-update',
  templateUrl: './application-update.component.html',
})
export class ApplicationUpdateComponent implements OnInit {
  isSaving = false;
  stepids: IStep[] = [];
  candidates: ICandidate[] = [];
  creationDateDp: any;

  editForm = this.fb.group({
    id: [],
    creationDate: [],
    newApplication: [],
    state: [],
    comment: [],
    stepId: [],
    candidateId: [],
  });

  constructor(
    protected applicationService: ApplicationService,
    protected stepService: StepService,
    protected candidateService: CandidateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ application }) => {
      this.updateForm(application);

      this.stepService
        .query({ filter: 'application-is-null' })
        .pipe(
          map((res: HttpResponse<IStep[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IStep[]) => {
          if (!application.stepId || !application.stepId.id) {
            this.stepids = resBody;
          } else {
            this.stepService
              .find(application.stepId.id)
              .pipe(
                map((subRes: HttpResponse<IStep>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IStep[]) => (this.stepids = concatRes));
          }
        });

      this.candidateService.query().subscribe((res: HttpResponse<ICandidate[]>) => (this.candidates = res.body || []));
    });
  }

  updateForm(application: IApplication): void {
    this.editForm.patchValue({
      id: application.id,
      creationDate: application.creationDate,
      newApplication: application.newApplication,
      state: application.state,
      comment: application.comment,
      stepId: application.stepId,
      candidateId: application.candidateId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const application = this.createFromForm();
    if (application.id !== undefined) {
      this.subscribeToSaveResponse(this.applicationService.update(application));
    } else {
      this.subscribeToSaveResponse(this.applicationService.create(application));
    }
  }

  private createFromForm(): IApplication {
    return {
      ...new Application(),
      id: this.editForm.get(['id'])!.value,
      creationDate: this.editForm.get(['creationDate'])!.value,
      newApplication: this.editForm.get(['newApplication'])!.value,
      state: this.editForm.get(['state'])!.value,
      comment: this.editForm.get(['comment'])!.value,
      stepId: this.editForm.get(['stepId'])!.value,
      candidateId: this.editForm.get(['candidateId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApplication>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
