import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICandidate, Candidate } from 'app/shared/model/candidate.model';
import { CandidateService } from './candidate.service';

@Component({
  selector: 'jhi-candidate-update',
  templateUrl: './candidate-update.component.html',
})
export class CandidateUpdateComponent implements OnInit {
  isSaving = false;
  bithDateDp: any;

  editForm = this.fb.group({
    id: [],
    driverslicense: [],
    mobility: [],
    city: [],
    address: [],
    cv: [],
    bithDate: [],
    handicapSituation: [],
    fullProfil: [],
    withoutExperience: [],
  });

  constructor(protected candidateService: CandidateService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ candidate }) => {
      this.updateForm(candidate);
    });
  }

  updateForm(candidate: ICandidate): void {
    this.editForm.patchValue({
      id: candidate.id,
      driverslicense: candidate.driverslicense,
      mobility: candidate.mobility,
      city: candidate.city,
      address: candidate.address,
      cv: candidate.cv,
      bithDate: candidate.bithDate,
      handicapSituation: candidate.handicapSituation,
      fullProfil: candidate.fullProfil,
      withoutExperience: candidate.withoutExperience,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const candidate = this.createFromForm();
    if (candidate.id !== undefined) {
      this.subscribeToSaveResponse(this.candidateService.update(candidate));
    } else {
      this.subscribeToSaveResponse(this.candidateService.create(candidate));
    }
  }

  private createFromForm(): ICandidate {
    return {
      ...new Candidate(),
      id: this.editForm.get(['id'])!.value,
      driverslicense: this.editForm.get(['driverslicense'])!.value,
      mobility: this.editForm.get(['mobility'])!.value,
      city: this.editForm.get(['city'])!.value,
      address: this.editForm.get(['address'])!.value,
      cv: this.editForm.get(['cv'])!.value,
      bithDate: this.editForm.get(['bithDate'])!.value,
      handicapSituation: this.editForm.get(['handicapSituation'])!.value,
      fullProfil: this.editForm.get(['fullProfil'])!.value,
      withoutExperience: this.editForm.get(['withoutExperience'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICandidate>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
