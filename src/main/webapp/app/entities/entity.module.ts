import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'offer',
        loadChildren: () => import('./offer/offer.module').then(m => m.LuminiskillsOfferModule),
      },
      {
        path: 'recruiter',
        loadChildren: () => import('./recruiter/recruiter.module').then(m => m.LuminiskillsRecruiterModule),
      },
      {
        path: 'company',
        loadChildren: () => import('./company/company.module').then(m => m.LuminiskillsCompanyModule),
      },
      {
        path: 'candidate',
        loadChildren: () => import('./candidate/candidate.module').then(m => m.LuminiskillsCandidateModule),
      },
      {
        path: 'experience',
        loadChildren: () => import('./experience/experience.module').then(m => m.LuminiskillsExperienceModule),
      },
      {
        path: 'training',
        loadChildren: () => import('./training/training.module').then(m => m.LuminiskillsTrainingModule),
      },
      {
        path: 'application',
        loadChildren: () => import('./application/application.module').then(m => m.LuminiskillsApplicationModule),
      },
      {
        path: 'step',
        loadChildren: () => import('./step/step.module').then(m => m.LuminiskillsStepModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class LuminiskillsEntityModule {}
