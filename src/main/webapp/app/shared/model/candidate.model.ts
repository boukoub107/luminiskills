import { Moment } from 'moment';
import { IApplication } from 'app/shared/model/application.model';
import { IExperience } from 'app/shared/model/experience.model';
import { ITraining } from 'app/shared/model/training.model';

export interface ICandidate {
  id?: number;
  driverslicense?: string;
  mobility?: string;
  city?: string;
  address?: string;
  cv?: string;
  bithDate?: Moment;
  handicapSituation?: boolean;
  fullProfil?: boolean;
  withoutExperience?: boolean;
  applications?: IApplication[];
  experiences?: IExperience[];
  trainings?: ITraining[];
}

export class Candidate implements ICandidate {
  constructor(
    public id?: number,
    public driverslicense?: string,
    public mobility?: string,
    public city?: string,
    public address?: string,
    public cv?: string,
    public bithDate?: Moment,
    public handicapSituation?: boolean,
    public fullProfil?: boolean,
    public withoutExperience?: boolean,
    public applications?: IApplication[],
    public experiences?: IExperience[],
    public trainings?: ITraining[]
  ) {
    this.handicapSituation = this.handicapSituation || false;
    this.fullProfil = this.fullProfil || false;
    this.withoutExperience = this.withoutExperience || false;
  }
}
