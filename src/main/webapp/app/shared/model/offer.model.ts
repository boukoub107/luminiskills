import { Moment } from 'moment';
import { IRecruiter } from 'app/shared/model/recruiter.model';
import { IApplication } from 'app/shared/model/application.model';

export interface IOffer {
  id?: number;
  creationDate?: Moment;
  lastUpdate?: Moment;
  contractTerm?: string;
  categoryId?: number;
  expirationDate?: Moment;
  activated?: boolean;
  description?: string;
  recruiterId?: IRecruiter;
  applicationId?: IApplication;
}

export class Offer implements IOffer {
  constructor(
    public id?: number,
    public creationDate?: Moment,
    public lastUpdate?: Moment,
    public contractTerm?: string,
    public categoryId?: number,
    public expirationDate?: Moment,
    public activated?: boolean,
    public description?: string,
    public recruiterId?: IRecruiter,
    public applicationId?: IApplication
  ) {
    this.activated = this.activated || false;
  }
}
