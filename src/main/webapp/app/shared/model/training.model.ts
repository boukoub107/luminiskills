import { Moment } from 'moment';
import { ICandidate } from 'app/shared/model/candidate.model';

export interface ITraining {
  id?: number;
  diploma?: string;
  university?: string;
  fromDate?: Moment;
  toDate?: Moment;
  city?: string;
  candidateId?: ICandidate;
}

export class Training implements ITraining {
  constructor(
    public id?: number,
    public diploma?: string,
    public university?: string,
    public fromDate?: Moment,
    public toDate?: Moment,
    public city?: string,
    public candidateId?: ICandidate
  ) {}
}
