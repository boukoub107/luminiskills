package com.luminiskills.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.luminiskills.web.rest.TestUtil;

public class RecruiterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Recruiter.class);
        Recruiter recruiter1 = new Recruiter();
        recruiter1.setId(1L);
        Recruiter recruiter2 = new Recruiter();
        recruiter2.setId(recruiter1.getId());
        assertThat(recruiter1).isEqualTo(recruiter2);
        recruiter2.setId(2L);
        assertThat(recruiter1).isNotEqualTo(recruiter2);
        recruiter1.setId(null);
        assertThat(recruiter1).isNotEqualTo(recruiter2);
    }
}
