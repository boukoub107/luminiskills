package com.luminiskills.web.rest;

import com.luminiskills.LuminiskillsApp;
import com.luminiskills.domain.Candidate;
import com.luminiskills.repository.CandidateRepository;
import com.luminiskills.service.CandidateService;
import com.luminiskills.service.dto.CandidateDTO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CandidateResource} REST controller.
 */
@SpringBootTest(classes = LuminiskillsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CandidateResourceIT {

    private static final String DEFAULT_DRIVERSLICENSE = "AAAAAAAAAA";
    private static final String UPDATED_DRIVERSLICENSE = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILITY = "AAAAAAAAAA";
    private static final String UPDATED_MOBILITY = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_CV = "AAAAAAAAAA";
    private static final String UPDATED_CV = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BITH_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BITH_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_HANDICAP_SITUATION = false;
    private static final Boolean UPDATED_HANDICAP_SITUATION = true;

    private static final Boolean DEFAULT_FULL_PROFIL = false;
    private static final Boolean UPDATED_FULL_PROFIL = true;

    private static final Boolean DEFAULT_WITHOUT_EXPERIENCE = false;
    private static final Boolean UPDATED_WITHOUT_EXPERIENCE = true;

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCandidateMockMvc;

    private Candidate candidate;
    private CandidateDTO cnd;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Candidate createEntity(EntityManager em) {
        Candidate candidate = new Candidate().driverslicense(DEFAULT_DRIVERSLICENSE).mobility(DEFAULT_MOBILITY)
                .city(DEFAULT_CITY).address(DEFAULT_ADDRESS).cv(DEFAULT_CV).bithDate(DEFAULT_BITH_DATE)
                .handicapSituation(DEFAULT_HANDICAP_SITUATION).fullProfil(DEFAULT_FULL_PROFIL)
                .withoutExperience(DEFAULT_WITHOUT_EXPERIENCE);
        return candidate;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Candidate createUpdatedEntity(EntityManager em) {
        Candidate candidate = new Candidate().driverslicense(UPDATED_DRIVERSLICENSE).mobility(UPDATED_MOBILITY)
                .city(UPDATED_CITY).address(UPDATED_ADDRESS).cv(UPDATED_CV).bithDate(UPDATED_BITH_DATE)
                .handicapSituation(UPDATED_HANDICAP_SITUATION).fullProfil(UPDATED_FULL_PROFIL)
                .withoutExperience(UPDATED_WITHOUT_EXPERIENCE);
        return candidate;
    }

    @BeforeEach
    public void initTest() {
        candidate = createEntity(em);
    }

    @Test
    @Transactional
    public void createCandidate() throws Exception {
        int databaseSizeBeforeCreate = candidateRepository.findAll().size();
        // Create the Candidate
        restCandidateMockMvc.perform(post("/api/candidates").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(candidate))).andExpect(status().isCreated());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeCreate + 1);
        Candidate testCandidate = candidateList.get(candidateList.size() - 1);
        assertThat(testCandidate.getDriverslicense()).isEqualTo(DEFAULT_DRIVERSLICENSE);
        assertThat(testCandidate.getMobility()).isEqualTo(DEFAULT_MOBILITY);
        assertThat(testCandidate.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testCandidate.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testCandidate.getCv()).isEqualTo(DEFAULT_CV);
        assertThat(testCandidate.getBithDate()).isEqualTo(DEFAULT_BITH_DATE);
        assertThat(testCandidate.isHandicapSituation()).isEqualTo(DEFAULT_HANDICAP_SITUATION);
        assertThat(testCandidate.isFullProfil()).isEqualTo(DEFAULT_FULL_PROFIL);
        assertThat(testCandidate.isWithoutExperience()).isEqualTo(DEFAULT_WITHOUT_EXPERIENCE);
    }

    @Test
    @Transactional
    public void createCandidateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = candidateRepository.findAll().size();

        // Create the Candidate with an existing ID
        candidate.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidateMockMvc.perform(post("/api/candidates").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(candidate))).andExpect(status().isBadRequest());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCandidates() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList
        restCandidateMockMvc.perform(get("/api/candidates?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(candidate.getId().intValue())))
                .andExpect(jsonPath("$.[*].driverslicense").value(hasItem(DEFAULT_DRIVERSLICENSE)))
                .andExpect(jsonPath("$.[*].mobility").value(hasItem(DEFAULT_MOBILITY)))
                .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
                .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
                .andExpect(jsonPath("$.[*].cv").value(hasItem(DEFAULT_CV)))
                .andExpect(jsonPath("$.[*].bithDate").value(hasItem(DEFAULT_BITH_DATE.toString())))
                .andExpect(
                        jsonPath("$.[*].handicapSituation").value(hasItem(DEFAULT_HANDICAP_SITUATION.booleanValue())))
                .andExpect(jsonPath("$.[*].fullProfil").value(hasItem(DEFAULT_FULL_PROFIL.booleanValue()))).andExpect(
                        jsonPath("$.[*].withoutExperience").value(hasItem(DEFAULT_WITHOUT_EXPERIENCE.booleanValue())));
    }

    @Test
    @Transactional
    public void getCandidate() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get the candidate
        restCandidateMockMvc.perform(get("/api/candidates/{id}", candidate.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(candidate.getId().intValue()))
                .andExpect(jsonPath("$.driverslicense").value(DEFAULT_DRIVERSLICENSE))
                .andExpect(jsonPath("$.mobility").value(DEFAULT_MOBILITY))
                .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
                .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS)).andExpect(jsonPath("$.cv").value(DEFAULT_CV))
                .andExpect(jsonPath("$.bithDate").value(DEFAULT_BITH_DATE.toString()))
                .andExpect(jsonPath("$.handicapSituation").value(DEFAULT_HANDICAP_SITUATION.booleanValue()))
                .andExpect(jsonPath("$.fullProfil").value(DEFAULT_FULL_PROFIL.booleanValue()))
                .andExpect(jsonPath("$.withoutExperience").value(DEFAULT_WITHOUT_EXPERIENCE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCandidate() throws Exception {
        // Get the candidate
        restCandidateMockMvc.perform(get("/api/candidates/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCandidate() throws Exception {
        // Initialize the database
        candidateService.save(cnd);

        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();

        // Update the candidate
        Candidate updatedCandidate = candidateRepository.findById(candidate.getId()).get();
        // Disconnect from session so that the updates on updatedCandidate are not
        // directly saved in db
        em.detach(updatedCandidate);
        updatedCandidate.driverslicense(UPDATED_DRIVERSLICENSE).mobility(UPDATED_MOBILITY).city(UPDATED_CITY)
                .address(UPDATED_ADDRESS).cv(UPDATED_CV).bithDate(UPDATED_BITH_DATE)
                .handicapSituation(UPDATED_HANDICAP_SITUATION).fullProfil(UPDATED_FULL_PROFIL)
                .withoutExperience(UPDATED_WITHOUT_EXPERIENCE);

        restCandidateMockMvc.perform(put("/api/candidates").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(updatedCandidate))).andExpect(status().isOk());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
        Candidate testCandidate = candidateList.get(candidateList.size() - 1);
        assertThat(testCandidate.getDriverslicense()).isEqualTo(UPDATED_DRIVERSLICENSE);
        assertThat(testCandidate.getMobility()).isEqualTo(UPDATED_MOBILITY);
        assertThat(testCandidate.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testCandidate.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testCandidate.getCv()).isEqualTo(UPDATED_CV);
        assertThat(testCandidate.getBithDate()).isEqualTo(UPDATED_BITH_DATE);
        assertThat(testCandidate.isHandicapSituation()).isEqualTo(UPDATED_HANDICAP_SITUATION);
        assertThat(testCandidate.isFullProfil()).isEqualTo(UPDATED_FULL_PROFIL);
        assertThat(testCandidate.isWithoutExperience()).isEqualTo(UPDATED_WITHOUT_EXPERIENCE);
    }

    @Test
    @Transactional
    public void updateNonExistingCandidate() throws Exception {
        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidateMockMvc.perform(put("/api/candidates").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(candidate))).andExpect(status().isBadRequest());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCandidate() throws Exception {
        // Initialize the database
        candidateService.save(cnd);

        int databaseSizeBeforeDelete = candidateRepository.findAll().size();

        // Delete the candidate
        restCandidateMockMvc
                .perform(delete("/api/candidates/{id}", candidate.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
