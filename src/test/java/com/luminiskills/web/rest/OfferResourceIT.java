package com.luminiskills.web.rest;

import com.luminiskills.LuminiskillsApp;
import com.luminiskills.domain.Offer;
import com.luminiskills.repository.OfferRepository;
import com.luminiskills.service.OfferService;
import com.luminiskills.service.dto.OfferDTO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OfferResource} REST controller.
 */
@SpringBootTest(classes = LuminiskillsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OfferResourceIT {

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CONTRACT_TERM = "AAAAAAAAAA";
    private static final String UPDATED_CONTRACT_TERM = "BBBBBBBBBB";

    private static final Integer DEFAULT_CATEGORY_ID = 1;
    private static final Integer UPDATED_CATEGORY_ID = 2;

    private static final LocalDate DEFAULT_EXPIRATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXPIRATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_ACTIVATED = false;
    private static final Boolean UPDATED_ACTIVATED = true;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferService offerService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOfferMockMvc;

    private Offer offer;
    private OfferDTO of;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Offer createEntity(EntityManager em) {
        Offer offer = new Offer().creationDate(DEFAULT_CREATION_DATE).lastUpdate(DEFAULT_LAST_UPDATE)
                .contractTerm(DEFAULT_CONTRACT_TERM).categoryId(DEFAULT_CATEGORY_ID)
                .expirationDate(DEFAULT_EXPIRATION_DATE).activated(DEFAULT_ACTIVATED).description(DEFAULT_DESCRIPTION);
        return offer;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Offer createUpdatedEntity(EntityManager em) {
        Offer offer = new Offer().creationDate(UPDATED_CREATION_DATE).lastUpdate(UPDATED_LAST_UPDATE)
                .contractTerm(UPDATED_CONTRACT_TERM).categoryId(UPDATED_CATEGORY_ID)
                .expirationDate(UPDATED_EXPIRATION_DATE).activated(UPDATED_ACTIVATED).description(UPDATED_DESCRIPTION);
        return offer;
    }

    @BeforeEach
    public void initTest() {
        offer = createEntity(em);
    }

    @Test
    @Transactional
    public void createOffer() throws Exception {
        int databaseSizeBeforeCreate = offerRepository.findAll().size();
        // Create the Offer
        restOfferMockMvc.perform(post("/api/offers").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(offer))).andExpect(status().isCreated());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeCreate + 1);
        Offer testOffer = offerList.get(offerList.size() - 1);
        assertThat(testOffer.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testOffer.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testOffer.getContractTerm()).isEqualTo(DEFAULT_CONTRACT_TERM);
        assertThat(testOffer.getCategoryId()).isEqualTo(DEFAULT_CATEGORY_ID);
        assertThat(testOffer.getExpirationDate()).isEqualTo(DEFAULT_EXPIRATION_DATE);
        assertThat(testOffer.isActivated()).isEqualTo(DEFAULT_ACTIVATED);
        assertThat(testOffer.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createOfferWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = offerRepository.findAll().size();

        // Create the Offer with an existing ID
        offer.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOfferMockMvc.perform(post("/api/offers").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(offer))).andExpect(status().isBadRequest());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllOffers() throws Exception {
        // Initialize the database
        offerRepository.saveAndFlush(offer);

        // Get all the offerList
        restOfferMockMvc.perform(get("/api/offers?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(offer.getId().intValue())))
                .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
                .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())))
                .andExpect(jsonPath("$.[*].contractTerm").value(hasItem(DEFAULT_CONTRACT_TERM)))
                .andExpect(jsonPath("$.[*].categoryId").value(hasItem(DEFAULT_CATEGORY_ID)))
                .andExpect(jsonPath("$.[*].expirationDate").value(hasItem(DEFAULT_EXPIRATION_DATE.toString())))
                .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }

    @Test
    @Transactional
    public void getOffer() throws Exception {
        // Initialize the database
        offerRepository.saveAndFlush(offer);

        // Get the offer
        restOfferMockMvc.perform(get("/api/offers/{id}", offer.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(offer.getId().intValue()))
                .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
                .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()))
                .andExpect(jsonPath("$.contractTerm").value(DEFAULT_CONTRACT_TERM))
                .andExpect(jsonPath("$.categoryId").value(DEFAULT_CATEGORY_ID))
                .andExpect(jsonPath("$.expirationDate").value(DEFAULT_EXPIRATION_DATE.toString()))
                .andExpect(jsonPath("$.activated").value(DEFAULT_ACTIVATED.booleanValue()))
                .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    @Transactional
    public void getNonExistingOffer() throws Exception {
        // Get the offer
        restOfferMockMvc.perform(get("/api/offers/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOffer() throws Exception {
        // Initialize the database
        offerService.save(of);

        int databaseSizeBeforeUpdate = offerRepository.findAll().size();

        // Update the offer
        Offer updatedOffer = offerRepository.findById(offer.getId()).get();
        // Disconnect from session so that the updates on updatedOffer are not directly
        // saved in db
        em.detach(updatedOffer);
        updatedOffer.creationDate(UPDATED_CREATION_DATE).lastUpdate(UPDATED_LAST_UPDATE)
                .contractTerm(UPDATED_CONTRACT_TERM).categoryId(UPDATED_CATEGORY_ID)
                .expirationDate(UPDATED_EXPIRATION_DATE).activated(UPDATED_ACTIVATED).description(UPDATED_DESCRIPTION);

        restOfferMockMvc.perform(put("/api/offers").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(updatedOffer))).andExpect(status().isOk());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);
        Offer testOffer = offerList.get(offerList.size() - 1);
        assertThat(testOffer.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testOffer.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testOffer.getContractTerm()).isEqualTo(UPDATED_CONTRACT_TERM);
        assertThat(testOffer.getCategoryId()).isEqualTo(UPDATED_CATEGORY_ID);
        assertThat(testOffer.getExpirationDate()).isEqualTo(UPDATED_EXPIRATION_DATE);
        assertThat(testOffer.isActivated()).isEqualTo(UPDATED_ACTIVATED);
        assertThat(testOffer.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingOffer() throws Exception {
        int databaseSizeBeforeUpdate = offerRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOfferMockMvc.perform(put("/api/offers").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(offer))).andExpect(status().isBadRequest());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOffer() throws Exception {
        // Initialize the database
        offerService.save(of);

        int databaseSizeBeforeDelete = offerRepository.findAll().size();

        // Delete the offer
        restOfferMockMvc.perform(delete("/api/offers/{id}", offer.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
